const languageMap = {
  en: {
    create_tm: 'create',
    modify_tm: 'modify',
  },
  ru: {
    create_tm: 'создано',
    modify_tm: 'изменено',
  },
}

export default languageMap
